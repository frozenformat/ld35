import pygame
import platforms

class Mob(pygame.sprite.Sprite):
    def __init__(self, x, y, walking_speed, jump_height, image):
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.gravity = 1 #Set the value of gravity in px/s (pixles/second)
        self.change_in_y = 0 #The speed on the X axis in px/s (pixles/second)
        self.change_in_x = 0 #The speed on the Y axis in px/s (pixles/second)
        self.walking_speed = walking_speed
        self.jump_height = jump_height
        super().__init__()

    def load_image(self,image):
        self.image = pygame.image.load(image)
        old_x = self.rect.x
        old_y = self.rect.y
        self.rect = self.image.get_rect()
        self.set_pos(old_x, old_y)
        print(self.rect.h,self.rect.w)
        
    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y
    
    def update(self,mob_group):
        lowest_y = 700
        highest_y = 0
        left_bounds = 0
        right_bounds = 700
        self.change_in_y -= self.gravity/2

        self.set_pos(self.rect.x - self.change_in_x, self.rect.y)

        if self.rect.left < left_bounds:
            self.rect.left = left_bounds
        elif self.rect.right > right_bounds:
            self.rect.right = right_bounds

        self.collide_mobs(mob_group, self.change_in_x, 0)

        self.set_pos(self.rect.x, self.rect.y - self.change_in_y)

        if self.rect.top < highest_y:
            self.rect.top = highest_y
        elif self.rect.bottom > lowest_y:
            self.rect.bottom = lowest_y

        self.collide_mobs(mob_group, 0, self.change_in_y)

    def collide_mobs(self, mob_group, x_change, y_change):
        for mob in mob_group.sprites():
            if mob is self:
                continue

            if self.rect.colliderect(mob.rect):
                if x_change < 0:
                    self.rect.right = mob.rect.left
                elif x_change > 0:
                    self.rect.left = mob.rect.right

                if y_change > 0:
                    self.rect.top = mob.rect.bottom
                elif y_change < 0:
                    self.rect.bottom = mob.rect.top

    def collision_platform(self):
        sprite_length = self.sprite_offset_length 
        sprite_width = self.sprite_offset_width
        #y + 1 - Goes Down; y - 1 goes up || x + 1 goes right ; x - 1 goes left
        if self.rect.collidepoint(self.rect.x, self.rect.y + sprite_width):
            print('COLLIISION ' + str(self))
            # if self.change_in_x < 0:
            #     self.rect.x += self.change_in_x
            #     self.change_in_x = 0
            # elif self.change_in_x > 0:
            #     self.rect.x -= self.change_in_x
            #     self.change_in_x = 0
            # if self.change_in_y < 0:
            #     self.change_in_y = -self.change_in_y

class Player(Mob):
    def __init__(self, image):
        super().__init__(0, 0, 5, 10, image)
        # self.gravity = 2 
        # self.gravity = 1.1
        print(self.image.get_size())

class Test_Enemy(Mob):
    def __init__(self, x, y, image):
        super().__init__(x, y, 5, 0, image)
        self.change_in_x = 5
        self.move = True

    def transform(self, image=None):
        super().load_image(image)
        self.move = False
        self.change_in_x = 0
        self.change_in_y = 0

    def update(self,mobs):
        if self.move:
            if self.change_in_x <= -6:
                self.change_in_x = 5 
            if self.change_in_x - self.gravity/9 < 0:
                self.change_in_x = -5
            if self.change_in_x == 0:
                self.change_in_x = -6
        super().update(mobs)
