import pygame
import math
import units

# try:
    # import units
# except ImportError:
    # import src.units as units

def game_loop():
    pygame.init()
    h_w = (700,700)
    is_running = True
    surface = pygame.Surface( h_w )
    window = pygame.display.set_mode(h_w)
    pygame.display.set_caption('test')
    clock = pygame.time.Clock()
    fps = 60
    try:
        player = units.Player('./player.png')
    except pygame.error:
        player = units.Player('src/player.png')
    player.set_pos(0, h_w[1])
    try:
        test_enemy = units.Test_Enemy(689, 689, './teste.png')
    except pygame.error:
        test_enemy = units.Test_Enemy(689, 689, 'src/teste.png')

    enemy_group = pygame.sprite.Group(test_enemy)
    mob_group = pygame.sprite.Group(player, *enemy_group.sprites())
    platform_group = pygame.sprite.Group()
    while is_running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_running = False
            elif event.type == pygame.KEYDOWN:
                if pygame.key.name(event.key) == 'down':
                    pass
                elif pygame.key.name(event.key) == 'up':
                    player.change_in_y = player.jump_height 
                elif pygame.key.name(event.key) == 'left':
                    player.change_in_x = player.walking_speed
                elif pygame.key.name(event.key) == 'right':
                    player.change_in_x = -player.walking_speed
                if pygame.key.name(event.key) == 't':
                    for unit in enemy_group.sprites():
                        distance = math.sqrt(((player.rect.centerx - unit.rect.centerx)**2) + 
                                             ((player.rect.centery - unit.rect.centery)**2))
                        if distance <= 400:
                            #unit.kill()
                            platform = unit.transform('./testblock.png')


        for platform in platform_group.sprites():
            for unit in enemy_group.sprites():
                platform.collision_platform(unit)
            platform.collision_platform(player)
        clock.tick(fps)
        pygame.display.update()
        window.fill((0,0,0))
        mob_group.update(mob_group)
        platform_group.update()

        mob_group.draw(window)
        platform_group.draw(window)
    pygame.quit()


if __name__ == "__main__":
    game_loop()
