import pygame

class Platform(pygame.sprite.Sprite):
    def __init__(self, image):
        self.image = pygame.image.load(image)
        self.rect = self.image.get_rect()
        self.sprite_offset_length = self.image.get_size()[0]
        self.sprite_offset_width = self.image.get_size()[1]
        self.gravity = 1 #Set the value of gravity for characters in px/s (pixles/second)
        self.change_in_y = 0 #The speed a mob on the X axis in px/s (pixles/second)
        self.change_in_x = 0 #The speed a mob on the Y axis in px/s (pixles/second)
        # self.walking_speed = walking_speed #The general speed of a mob on this area
        # self.jump_height = jump_height #The general height of a jump on this surface
        super().__init__()
        
    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y
    
    def update(self):
        lowest_y = 700 - self.sprite_offset_width
        highest_y = (700 - self.sprite_offset_width) + 100
        left_bounds = 0
        right_bounds = 700 - self.sprite_offset_length
        self.set_pos(self.rect.x - self.change_in_x, self.rect.y - self.change_in_y)
        if self.change_in_y > 0 and self.rect.y < lowest_y:
          self.change_in_y -= self.gravity/2 

        if self.change_in_x > 0:
            self.change_in_x -= self.gravity/9
            if self.change_in_x < 0:
                self.change_in_x = 0
        elif self.change_in_x < 0:
            self.change_in_x += self.gravity/9
            if self.change_in_x > 0:
                self.change_in_x = 0

        if self.rect.x < left_bounds:
            self.rect.x += (-self.rect.x + left_bounds)
        elif self.rect.x > right_bounds:
            self.rect.x -= (self.rect.x - right_bounds)
        if self.rect.x == (left_bounds - 1) or self.rect.x == right_bounds:
            self.change_in_x = 0 

        if self.rect.y > lowest_y:
            self.rect.y -= (self.rect.y - lowest_y)
        elif self.rect.y < lowest_y and self.change_in_y == 0:
            self.change_in_y += self.gravity*-4

        if self.rect.y == lowest_y:
            self.change_in_y = 0

    def collision_platform(self, mob):
        sprite_length = self.sprite_offset_length 
        sprite_width = self.sprite_offset_width
        #y + 1 - Goes Down; y - 1 goes up || x + 1 goes right ; x - 1 goes left
        if self.rect.colliderect(mob.rect):
            if mob.rect.x >= self.rect.x and mob.rect.x <= (self.rect.x + self.sprite_offset_length):
                mob.rect.y = self.rect.y - mob.sprite_offset_width 
                if mob.change_in_y < 0:
                    mob.change_in_y = 0
                if mob.change_in_y > 0:
                    mob.rect.y += 10
            # if self.change_in_x < 0:
            #     self.rect.x += self.change_in_x
            #     self.change_in_x = 0
            # elif self.change_in_x > 0:
            #     self.rect.x -= self.change_in_x
            #     self.change_in_x = 0
            # if self.change_in_y < 0:
            #     self.change_in_y = -self.change_in_y

class Test_Platform(Platform):
    def __init__(self, image):
        pass
